#####
## Prediction modelo definition
#
ModeloDiscriminacao <- function(dadosTreino, dadosTeste, rankingDiscriminantes){
  
  # Inicialize variables
  discriminantesEleitas <- character()
  maiorAcuracia <- 0
  sensitivity <- 0
  specificity <- 0
  
  # Processes the discriminants from the
  for(discriminante in rankingDiscriminantes){
    
    # Initialize selected discriminants
    discriminantesEleitas <- c(discriminantesEleitas, discriminante)
    
    # Creates kNN model
    modelo <- dadosTreino %>%
      select(autentico, !!discriminantesEleitas) %>% 
      KnnClassico()
    
    # Computes accuracy
    resultados <- VerificaAcuracia(
      modelo = modelo,
      dadosTeste = dadosTeste %>%
        select(autentico, !!discriminantesEleitas)
    )
    
    # If the average is greater
    if(resultados$Accuracy > maiorAcuracia){
      
      # Get accuracy
      maiorAcuracia <- resultados$Accuracy
      
      # Further results
      sensitivity <- resultados$Sensitivity
      specificity <- resultados$Specificity
      
      # If reached accuracy limit
      if(maiorAcuracia == 1){
        break()
      }
      
      # Search next candidate
      next()
    } 
    
    # Removes selected discriminants
    discriminantesEleitas <- discriminantesEleitas[discriminantesEleitas != discriminante]
    
  }
  
  return(
    list(
      Model = modelo,
      Accuracy = maiorAcuracia,
      Sensitivity = sensitivity,
      Specificity = specificity,
      Variables = discriminantesEleitas,
      QtdVariables = length(discriminantesEleitas),
      RetainedVariables = length(discriminantesEleitas) / ncol(dadosTreino)
    )
  )
}

####
## End
#